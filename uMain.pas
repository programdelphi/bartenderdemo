unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, FireDAC.Stan.StorageBin, Vcl.Mask, FireDAC.Stan.StorageJSON,
  JvComponentBase, JvDBGridExport, Vcl.AppEvnts, frxClass, frxDBSet, Vcl.ToolWin, Vcl.ComCtrls, frxDesgn
  ;

type
  TfrmMain = class(TForm)
    FDMemTable1: TFDMemTable;
    DataSource1: TDataSource;
    DBNavigator1: TDBNavigator;
    JvDBGridCSVExport1: TJvDBGridCSVExport;
    dbgrd1: TDBGrid;
    btn1: TButton;
    frxdbdtst1: TfrxDBDataset;
    ToolBar1: TToolBar;
    frxDesigner1: TfrxDesigner;
    frxrprt1: TfrxReport;
    btn2: TButton;
    btn3: TButton;
    pnlBT16: TPanel;
    stat1: TStatusBar;
    pnlBTedt: TPanel;
    pnlBtns: TPanel;
    btnExportPrint: TButton;
    btnExport: TButton;
    btnPrintBtwLegacy: TButton;
    btn4: TButton;
    edtBtwFile: TEdit;
    edtCSV: TEdit;
    edtEXE: TEdit;
    btn5: TButton;
    pnl2: TPanel;
    edtFrxFile: TEdit;
    btn6: TButton;
    pnl1: TPanel;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure btnExportPrintClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //procedure btn1Click(Sender: TObject);
      procedure FormShow(Sender: TObject);
  private
    procedure LoadData;
    procedure SaveData;
    procedure btPrntIt(cFormat, cDataFile: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses ShellApi;

{$R *.dfm}


procedure TfrmMain.btn1Click(Sender: TObject);
begin
frxrprt1.FileName :=  edtFrxFile.text;
frxrprt1.LoadFromFile(edtFrxFile.Text) ;
frxrprt1.PrepareReport() ;
frxrprt1.ShowReport(True) ;


end;

procedure TfrmMain.btn2Click(Sender: TObject);
begin
frxrprt1.LoadFromFile(edtFrxFile.text) ;
frxrprt1.DesignReport()   ;
end;

procedure TfrmMain.btn3Click(Sender: TObject);
begin
if edtFrxFile.Text ='' then
begin
  ShowMessage ('Fill FRX Path to continue') ;
  exit;
end;
  frxrprt1.LoadFromFile(edtFrxFile.Text) ;
  frxrprt1.PrepareReport() ;
  frxrprt1.Print  ;
  stat1.Panels[0].Text := 'Last Action: FRX Printed' ;
end;

procedure TfrmMain.btn4Click(Sender: TObject);
begin
  btPrntIt(edtBtwFile.Text,edtCSV.Text) ;
end;

procedure TfrmMain.btn5Click(Sender: TObject);
begin
edtBtwFile.text  :='C:\Temp\MemTableDemo\DemoTest.btw';
edtCSV.Text      :='C:\Temp\MemTableDemo\CsvExport' ;
edtEXE.Text      :='C:\Program Files\Seagull\BarTender 2019\BarTend.exe' ;
end;

procedure TfrmMain.btn6Click(Sender: TObject);
begin
edtFrxFile.Text := 'C:\Temp\MemTableDemo\Win32\Debug\DemoTest.fr3'
end;

procedure TfrmMain.btnExportClick(Sender: TObject);
begin
stat1.Panels[0].Text := 'Last Action: Export CSV' ;
JvDBGridCSVExport1.ExportGrid ;
ShowMessage ('Data Exported');
end;

procedure TfrmMain.btnExportPrintClick(Sender: TObject);
begin
   JvDBGridCSVExport1.ExportGrid ;
     btPrntIt(edtBtwFile.Text,edtCSV.Text) ;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 SaveData ;
end;





procedure TfrmMain.FormShow(Sender: TObject);
begin
   LoadData ;
end;

procedure TfrmMain.LoadData();
begin
      FDMemTable1.LoadFromFile('BTWtesterdata', sfJSON) ;
end;

procedure TfrmMain.SaveData;
begin
  FDMemTable1.Active := True;
  FDMemTable1.SaveToFile('BTWtesterdata', sfJSON);
end;

   procedure TfrmMain.btPrntIt(cFormat, cDataFile: string);
var
  LabelPath, cSysTray, cParam: string;
  pParam: PChar;
  Exe: PChar;
  AShowOption: Integer;
  FbtPath: string;

  I: Integer;
begin
    FbtPath:=(edtEXE.Text) ;
    LabelPath := '/F='+cFormat;
  // label path and name
  // LabelPath := FFormatPath + cFormat;
  // exe param
  // pParam := PChar('/F=' + LabelPath + ' /DD /P'); // PD will show Printer Dialog

 { if ShowPrintDialog then
  begin
    cParam := LabelPath + '/D="' + cDataFile + '" /PD';
    AShowOption := SW_MINIMIZE;
  end
  else
  begin
  }
    cSysTray :=  '/MIN=SystemTray' ;
    cParam := '/P ' + LabelPath + '/D="' + cDataFile + '" ' + cSysTray;
    AShowOption := SW_HIDE;
//  end;
 // if PrinterName <> '' then
 //   cParam := cParam + ' /PRN="' + PrinterName + '"';
  pParam := PChar(cParam);
  Exe := PChar(FbtPath);
  if not FileExists(FbtPath) then
  begin
        ShowMessage('BarTend.exe not found. (' + FbtPath +')');
        Exit;
  end;
  ShellExecute(Application.Handle, 'OPEN', Exe, pParam, '', AShowOption);
end;

end.
