unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.StorageBin, Vcl.StdCtrls,
  Vcl.ExtCtrls, JvComponentBase, JvDBGridExport, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, Vcl.Grids,
  Vcl.DBGrids;

type
  TfrmMain = class(TForm)
    dbgrd1: TDBGrid;
    DBNavigator1: TDBNavigator;
    DataSource1: TDataSource;
    FDMemTable1: TFDMemTable;
    JvDBGridCSVExport1: TJvDBGridCSVExport;
    pnlBT16: TPanel;
    pnlBTedt: TPanel;
    edtBtwFile: TEdit;
    edtCSV: TEdit;
    edtEXE: TEdit;
    Panel1: TPanel;
    Button5: TButton;
    btnExport: TButton;
    btnPrintBtwLegacy: TButton;
    btn4: TButton;
    btn5: TButton;
    lblInfo: TLabel;
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
  private
    procedure btPrntIt(cFormat, cDataFile: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses ShellApi;

{$R *.dfm}

procedure TfrmMain.btn4Click(Sender: TObject);
begin
btPrntIt(edtBtwFile.Text,edtCSV.Text) ;
end;

procedure TfrmMain.btn5Click(Sender: TObject);
begin
edtBtwFile.text  :='C:\Temp\MemTableDemo\DemoTest.btw';
edtCSV.Text      :='C:\Temp\MemTableDemo\CsvExport' ;
edtEXE.Text      :='C:\Program Files\Seagull\BarTender 2019\BarTend.exe' ;
end;

procedure TfrmMain.btnExportClick(Sender: TObject);
begin
  JvDBGridCSVExport1.ExportGrid ;
  ShowMessage ('Data Exported');
end;

procedure TfrmMain.btPrntIt(cFormat, cDataFile: string);
var
  LabelPath, cSysTray, cParam: string;
  pParam: PChar;
  Exe: PChar;
  AShowOption: Integer;
  FbtPath: string;

  I: Integer;
begin
    FbtPath:=(edtEXE.Text) ;
    LabelPath := '/F='+cFormat;
  // label path and name
  // LabelPath := FFormatPath + cFormat;
  // exe param
  // pParam := PChar('/F=' + LabelPath + ' /DD /P'); // PD will show Printer Dialog

 { if ShowPrintDialog then
  begin
    cParam := LabelPath + '/D="' + cDataFile + '" /PD';
    AShowOption := SW_MINIMIZE;
  end
  else
  begin
  }
    cSysTray :=  '/MIN=SystemTray' ;
    cParam := '/P ' + LabelPath + '/D="' + cDataFile + '" ' + cSysTray;
    AShowOption := SW_HIDE;
//  end;
 // if PrinterName <> '' then
 //   cParam := cParam + ' /PRN="' + PrinterName + '"';
  pParam := PChar(cParam);
  Exe := PChar(FbtPath);
  if not FileExists(FbtPath) then
  begin
        ShowMessage('BarTend.exe not found. (' + FbtPath +')');
        Exit;
  end;
  ShellExecute(Application.Handle, 'OPEN', Exe, pParam, '', AShowOption);
end;

end.


